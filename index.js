require('dotenv').config();
var Git = require("nodegit");
var pathToRepo = require("path").resolve(`${process.env.REPO}`);
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var http = require('http');
var server = http.createServer(app);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())

//Handle incoming POST - Get last commit message from repo
app.post('/getlastcommit', function(req, res) {
    var getMostRecentCommit = function(repository) {
        return repository.getBranchCommit("develop");
    };

    var getCommitMessage = function(commit) {
        return commit.message();
    };

    Git.Repository.open(pathToRepo)
        .then(getMostRecentCommit)
        .then(getCommitMessage)
        .then(function(message) {
          res.send(message);
    }).catch(function(e){
        console.log(e);
        res.send(JSON.stringify(e));
    });
});

//Get last commit message from repo
app.post('/pullByHash', function(req, res) {
    console.log("HASH", req.body.query);
    Git.Repository.open(pathToRepo)
        //FETCH
        .then(function(repo) {
            return repo.fetch("origin");
        })
        .then(function(){
            return repo.getCommit(req.body.query);
        })
        .then(getCommitMessage)
        .then(function(message) {
          res.send(message);
    }).catch(function(e){
        console.log(e);
        res.send(JSON.stringify(e));
    });
});

app.post('/getTagList', function(req, res){
    Git.Repository.open(pathToRepo)
    .then(function (repo) {
        return Git.Tag.list(repo).then(function(array) {
            res.send(JSON.stringify(array));
        });
    });
});

//Pull the commit by hash
app.post('/pullByTag', function(req, res) {
    console.log("TAG", req.body.query);
    
    Git.Repository.open(pathToRepo).then(function (repo) {
        return Git.Tag.list(repo)
            .then(function(array) {
                console.log(array);
                // array is ['v1.0.0','v2.0.0']
                return Git.Tag.lookup(repo, array[0])
                    .then(function(tag) {
                        console.log("TAG HASH", tag);
                        // return Git.Checkout.tree(repo, tag.targetId(), { checkoutStrategy: Checkout.STRATEGY.SAFE_CREATE})
                        // .then(function() {
                        //     repo.setHeadDetached(tag.targetId(), repo.defaultSignature, "Checkout: HEAD " + tag.targetId());
                        //     res.send("Checkout: HEAD " + tag.targetId());
                        // });
                    });
        }).catch(function(e){
            console.log(e);
        })
    }).catch(function(e){
        console.log(e);
        res.send(JSON.stringify(e));
    });
});

// TEST Endpoint, sends back the query
app.post('/echo', function(req, res) {
    res.send(req.body.query);
});

// TEST Endpoint, sends back the query
app.post('/', function(req, res) {
    res.send("It's alive.");
});

//Serve HTML on load
app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

server.listen(1234);